const express = require('express');
const users = express();
var path = require('path');
var bodyParser = require('body-parser');
var dbConn = require('./conexaoBD');
const { errorMonitor } = require('events');
const { error } = require('console');

users.use(bodyParser.json());
users.use(bodyParser.urlencoded({
    extended: true
}));
//rotas usuarios
//-----------------------------------------------------------------
// idUsuario, primeiroNome, sobrenome, email, senha, telefone, endereco, cidade, regiao, idade, cartaoSUS, cpf, sexo

users.post('/logar', function (req, res) {

    let email = req.body.emails;
    let senha = req.body.pass;
    if (!email) {
        return res.status(400).send({ error:true, message: 'Sem dados!' });
    }
    dbConn.query("SELECT idUsuario FROM usuario where email = ? and senha = ? ", [email, senha], function (error, results, fields) {
        if (error) throw error;
        if(results.length > 0){
           //res.json({
           //    status: 200,
           //    user: results[0],
           //    mensagem: "Seja Bem Vindo"
           //});
           var usuario_logado = {
             status: 200,
             usuario: results,
             mensagem: "Seja Bem Vindo"
           };
           res.render('escolha-consulta-dados',{usuario_logado});
        }
        else{
            res.json({
                status: 200,
                Usuario: results[0],
                mensagem: "Usuario não retornado com sucesso"
            });
            res.end();
        }
    });
});

users.get('/edicao-dados/:id', function(req, res){
    const id_usuario = req.params.id;
   
    console.log("Id é "+id_usuario);
    if(!id_usuario){
        return res.status(400).send({error: true, message:'Sem id'})
    }

    var usuario_editar={
        id: id_usuario
    };

    dbConn.query("SELECT *  FROM usuario WHERE idUsuario = ? ", [id_usuario], function (error, results, fields) {
        if (error) throw error;

        var usuario_retornado={
            status: 200,
            usuario: results,
            mensagem: 'Editando dados para:',
            identificador: req.params.id
        };
        res.render('editar-cadastro',{usuario_retornado,id_usuario});
    });


});

users.get('/tela-login', function (req, res) {
    const dadosUsers={
        tituloPagina: "Login do paciente:"
    };
    res.render('login',{dadosUsers});
});

users.get('/cadastro-usuarios', function (req, res) {
    dbConn.query('SELECT idUsuario, primeiroNome, sobrenome, email, senha, telefone, endereco, cidade, regiao, idade, cartaoSUS, cpf, sexo FROM usuario', function (error, results, fields) {
        if (error) throw  error;
        const dadosUsers={
            tituloPagina: "Cadastro do paciente:",
            Horarios: results
        }
        res.render('index-cadastro',{dadosUsers});
    })
});

users.post('/edicao-dados/:id', function (req, res) {
    let idUsuario = req.params.id;
    console.log("Id dados:"+idUsuario);
    if (!idUsuario) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }

    var usuario = {
		primeiroNome: req.body.nome,
		sobrenome: req.body.sobrenome,
		email: req.body.email,
        senha: req.body.senha,
        senha: req.body.confirmar_senha,
        telefone: req.body.telefone,
        endereco: req.body.endereco,
        cidade: req.body.cidade,
        regiao: req.body.regiao,
        idade: req.body.idadePaciente,
        cartaoSUS: req.body.cartSUS,
        cpf: req.body.cpf,
        sexo: req.body.sexo
	};
    if(usuario.senha == usuario.confirmar_senha){
        dbConn.query('UPDATE usuario Set primeiroNome =?, sobrenome = ?, email = ?, senha = ?, telefone = ?, endereco = ?, cidade = ?, regiao = ?, idade = ?, cartaoSUS = ?, cpf = ?, sexo = ? Where idUsuario=?', [usuario.nome, usuario.sobrenome, usuario.email, usuario.senha, usuario.telefone, usuario.endereco, usuario.cidade, usuario.regiao, usuario.idadePaciente, usuario.cartSUS, usuario.cpf, usuario.sexo, idUsuario], function (error, results, fields) {
            if (error) {
                return res.send({ error: true, data: results, message: error });
            };
            return res.json(
                {
                    error: false,
                    data: results,
                    message: 'Usuário atualizado!.',
	            	id: idUsuario
	            });
            })
    }

  ;
});

users.get('/todos-usuarios',function(req, res){
    dbConn.query('SELECT idUsuario, primeiroNome, sobrenome, email, senha, telefone, endereco, cidade, regiao, idade, cartaoSUS, cpf, sexo FROM usuario',function(error, results, fields){
        if(error){
            return res.status(400).send({ error: true, message: 'Usuario nao fornecido' });
        }
        res.json({
            Status: 200,
            Usuarios: results,
            Mensagem: "Lista de usuarios retornada!"
        });
        res.end();
    });
});

users.get('/usuario/:id', function (req, res) {
    let usuario_id = req.params.id;
    if (!usuario_id) {
     	return res.status(400).send({ error: true, message: 'Usuario nao fornecido' });
    }
    	dbConn.query('SELECT idUsuario, primeiroNome, sobrenome, email, senha, telefone, endereco, cidade, regiao, idade, cartaoSUS, cpf, sexo FROM usuario where idUsuario=?', usuario_id, function (error, results, fields) {
     		if (error) throw error;

     		res.json({
      	 		status: 200,
     	   		Usuario: results[0],
        		mensagem: "Usuario retornado com sucesso"
    		});
    	res.end();
    	});
});

users.post('/usuario', function (req, res) {
    let primeiroNome = req.body.nome;

    if (!primeiroNome) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }

	var usuario = {
		primeiroNome: req.body.nome,
		sobrenome: req.body.sobrenome,
		email: req.body.email,
        senha: req.body.senha,
        telefone: req.body.telefone,
        endereco: req.body.endereco,
        cidade: req.body.cidade,
        regiao: req.body.regiao,
        idade: req.body.idadePaciente,
        cartaoSUS: req.body.cartSUS,
        cpf: req.body.cpf,
        sexo: req.body.sexo,
        message: 'Novo usuário adicionado!'
	};

    dbConn.query('insert into usuario (primeiroNome, sobrenome, email, senha, telefone, endereco, cidade, regiao, idade, cartaoSUS, cpf, sexo) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
     [usuario.primeiroNome, usuario.sobrenome, usuario.email, usuario.senha, usuario.telefone, usuario.endereco, usuario.cidade, usuario.regiao, usuario.idade,usuario.cartaoSUS, usuario.cpf,usuario.sexo],
     function (error, results, fields) {
 	if (error) {
    	return res.send({ error: true, data: results, message: error });
  	};
      res.render('resposta_usuario',{usuario});
    });
});

users.delete('/usuario/:id', function (req, res) {
    let idUsuario = req.params.id;
    if (!idUsuario) {
        return res.status(400).send({ error: true, message: 'Sem id' });
    }
    dbConn.query('DELETE FROM usuario WHERE idUsuario = ?', [idUsuario], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Usuário removido com sucesso.' });
    });
});

module.exports = users;
