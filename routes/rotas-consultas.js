const express = require('express');
const consulta = express();
var path = require('path');
var bodyParser = require ('body-parser');
var dbConn = require('./conexaoBD');
consulta.use(bodyParser.json());
consulta.use(bodyParser.urlencoded({
    extended: true
}));

//rotas consultas
//-----------------------------------------------------------------
consulta.get('/todas-consultas',function(req, res){
    dbConn.query('SELECT c.idConsulta, h.idhorarios, h.diaSemana ,h.horario ,h.localidade, h.nomeMedico, c.idpaciente, u.primeiroNome, u.sobrenome, u.email, u.telefone, u.endereco, u.regiao, u.cpf FROM consulta AS c, horarios AS h, usuario AS u WHERE c.idhorario = h.idhorarios AND u.idUsuario=c.idpaciente ',
    function(error, results, fields){
        if(error) throw error;
        res.json({
            Status: 200,
            Consultas: results,
            Mensagem: "Lista de consultas retornada!"
        });
        res.end();
    });
});
consulta.get('/tela-consulta/:id', function (req, res) {
    let idUsuario = req.params.id;
    
    dbConn.query('SELECT idhorarios, diaSemana, nomeMedico, localidade, horario FROM horarios', function (error, results, fields) {
        if (error) throw  error;
        var dadosHorarios = {
            tituloPagina: "Lista de Horários",
            Horarios: results
        };
        dbConn.query('SELECT idUsuario, primeiroNome, sobrenome, email, senha, telefone, endereco, cidade, regiao, idade, cartaoSUS, cpf, sexo FROM usuario where idUsuario=?',[idUsuario],function(error,results,fields){
            if(error) throw error;
            var user = {
                status:200,
                txt:"Usuário logado",
                horario: results[0]
            };
            res.render('consulta',[dadosHorarios, user]);
        });
    });
});

consulta.get('/consulta/:id', function (req, res) {
    let consulta_id = req.params.id;
    if (!consulta_id) {
     	return res.status(400).send({ error: true, message: 'Consulta nao fornecido' });
    }
    	dbConn.query('SELECT c.idConsulta, h.idhorarios, h.diaSemana ,h.horario ,h.localidade, h.nomeMedico, c.idpaciente, u.primeiroNome, u.sobrenome, u.email, u.telefone, u.endereco, u.regiao, u.cpf FROM consulta AS c, horarios AS h, usuario AS u WHERE c.idhorario = h.idhorarios AND u.idUsuario=c.idpaciente AND idConsulta=?', consulta_id, function (error, results, fields) {
     		if (error) throw error;

     		res.json({
      	 		status: 200,
     	   		horario: results[0],
        		mensagem: "Horario retornado com sucesso"
    		});
    	res.end();
    	});
});

consulta.post('/consulta', function (req, res) {
    let id_horario = req.body.idhorario;

    if (!id_horario) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }

	var consulta = {
		idhorario: req.body.idhorario,
		idpaciente: req.body.idpaciente
	};

	dbConn.query('insert into consulta (idhorario, idpaciente) VALUES (?, ?) ', [consulta.idhorario,consulta.idpaciente], function (error, results, fields) {
 	if (error) {
    	return res.send({ error: true, data: results, message: error });
  	};
    	return res.send(
        {
            error: false,
            data: results,
            message: 'Nova consulta adicionada!.',
			id: results.insertId
		});
    });
});

consulta.put('/consulta/:id', function (req, res) {
    let id_consulta = req.params.id;

    if (!id_consulta) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }

	var consulta = {
		idhorario: req.body.idhorario,
		idpaciente: req.body.idpaciente
	};


   dbConn.query('UPDATE consulta SET idhorario =?, idpaciente = ? WHERE idconsulta=?', [consulta.idhorario, consulta.idpaciente, id_consulta], function (error, results, fields) {
  if (error) {
    return res.send({ error: true, data: results, message: error });
  };
    return res.send(
        {
            error: false,
            data: results,
            message: 'Consulta atualizada coms sucesso!.',
			id: id_consulta
		});
    });
});

consulta.delete('/consulta/:id', function (req, res) {
    let id_consulta = req.params.id;
    if (!id_consulta) {
        return res.status(400).send({ error: true, message: 'Sem id' });
    }
    dbConn.query('DELETE FROM consulta WHERE idconsulta = ?', [id_consulta], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Consulta removido com sucesso.' });
    });
});

module.exports = consulta;
