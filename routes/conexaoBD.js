var mysql = require('mysql');
var dbConn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'consultarapida'
});

// Conecta ao BD
dbConn.connect();
module.exports = dbConn;