const express = require('express');
const app = express();
var path = require('path');
var bodyParser = require ('body-parser');
var dbConn = require('./conexaoBD');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));



//rotas horarios
//------------------------------------------------------------------------------------------------------------------------------------------
app.get('/todos-horarios', function (req, res) {
    dbConn.query('SELECT idhorarios, diaSemana, nomeMedico, localidade, horario FROM horarios', function (error, results, fields) {
        if (error) throw  error;
        const dadosHorarios={
            tituloPagina: "Lista de Horários",
            Horarios: results
        }
        res.render('ver-horarios',{dadosHorarios});
    })
});

app.get('/horario/:id', function (req, res) {
    let horario_id = req.params.id;
    if (!horario_id) {
     	return res.status(400).send({ error: true, message: 'Horario nao fornecido' });
    }
    	dbConn.query('SELECT idhorarios, diaSemana, nomeMedico, localidade, horario FROM horarios where idhorarios=?', horario_id, function (error, results, fields) {
     		if (error) throw error;

     		res.json({
      	 		status: 200,
     	   		horario: results[0],
        		mensagem: "Horario retornado com sucesso"
    		});
    	res.end();     
    	});
});

app.post('/horario', function (req, res) {
    let nomeMedico = req.body.medico;

    if (!nomeMedico) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }
    
	var horario = {
        nomeMedico: req.body.medico,
        horario: req.body.horario,
        diaSemana: req.body.diaSemana,
        localidade: req.body.localidade,
        message: 'Novo horario adicionado com sucesso!.'
	};

	dbConn.query('insert into horarios (nomeMedico, diaSemana, horario, localidade) values (?, ?, ?, ?)', [horario.nomeMedico, horario.diaSemana, horario.horario, horario.localidade], function (error, results, fields) {
 	if (error) {
    	return res.send({ error: true, data: results, message: error });  
  	};
        res.render('respostas',{horario});
    });
    
});

app.put('/horario/:id', function (req, res) {
    let id_horarios = req.params.id;
   
    if (!id_horarios) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }

    var horario = {
        nomeMedico: req.body.nomeMedico,
		localidade: req.body.localidade,
		horario: req.body.horario,
		diaSemana: req.body.diaSemana
    };

   dbConn.query('UPDATE horarios Set nomeMedico =?, localidade = ?, horario = ?, diaSemana = ? Where idhorarios=?', [horario.nomeMedico, horario.localidade, horario.horario, horario.diaSemana, id_horarios], function (error, results, fields) {
  if (error) {
    return res.send({ error: true, data: results, message: error });  
  };
    return res.send(
        { 
            error: false, 
            data: results, 
            message: 'Horário atualizado!.',
			id: id_horarios 
		});
    });
});
 
app.delete('/horario/:id', function (req, res) {
    let id_horarios = req.params.id;
    if (!id_horarios) {
        return res.status(400).send({ error: true, message: 'Sem id' });
    }
    dbConn.query('DELETE FROM horarios WHERE idhorarios = ?', [id_horarios], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Horario removido com sucesso.' });
    });
});










// Rotas diferentes
app.get('/delete/:id', function (req, res) {
    let id_horarios = req.params.id;
    if (!id_horarios) {
        return res.status(400).send({ error: true, message: 'Sem id' });
    }
    dbConn.query('DELETE FROM horarios WHERE idhorarios = ?', [id_horarios], function (error, results, fields) {
        if (error) throw error;
        var info = {
            message: 'Deletado com sucesso!'
        }
        res.render('sucesso',{info});
    });
});

app.get('/edicao/:id', function(req, res){
    let id_horario = req.params.id;
    if(!id_horario){
        return res.status(400).send({error: true, message:'Sem id'})
    }
    
    var horario_editar={
        id: req.params.id
    };

    dbConn.query('SELECT idhorarios, diaSemana, nomeMedico, localidade, horario FROM horarios where idhorarios=?', horario_editar.id, function (error, results, fields) {
        if (error) throw error;

        var horario_retornado={
            status: 200,
            horario: results,
            mensagem: 'Preencha os campos para edição:',
            identificador: req.params.id
        };
        res.render('editar-horario',{horario_retornado});    
    });
    
   
});

app.get('/horario/:id', function (req, res) {
    let id_horarios = req.params.id;
   
    if (!id_horarios) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }

    var horario = {
        nomeMedico: req.body.nomeMedico,
		localidade: req.body.localidade,
		horario: req.body.horario,
		diaSemana: req.body.diaSemana
    };

   dbConn.query('UPDATE horarios Set nomeMedico =?, localidade = ?, horario = ?, diaSemana = ? Where idhorarios=?', [horario.nomeMedico, horario.localidade, horario.horario, horario.diaSemana, id_horarios], function (error, results, fields) {
  if (error) {
    return res.send({ error: true, data: results, message: error });  
  };
    return res.send(
        { 
            error: false, 
            data: results, 
            message: 'Horário atualizado!.',
			id: id_horarios 
		});
    });
});


app.post('/novohorario/:id', function (req, res) {
    let id_horarios = req.params.id;
    if (!id_horarios) {
      return res.status(400).send({ error:true, message: 'Sem dados!' });
    }
    
	var horario = {
        id: req.params.id,
        nomeMedico: req.body.medico,
        horario: req.body.horario,
        diaSemana: req.body.diaSemana,
        localidade: req.body.localidade,
        message: 'Horário editado com sucesso!'
	};

	dbConn.query('UPDATE horarios Set nomeMedico =?, localidade = ?, horario = ?, diaSemana = ? Where idhorarios=?', [horario.nomeMedico, horario.localidade, horario.horario, horario.diaSemana, id_horarios], function (error, results, fields) {
 	if (error) {
    	return res.send({ error: true, data: results, message: error });  
  	};
        res.render('respostas',{horario});
    });
    
});

module.exports = app;