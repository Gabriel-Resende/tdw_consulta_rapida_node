const express = require('express');
const app = express();

var path = require('path');
var mysql = require('mysql');

const port = 8080;
var bodyParser = require ('body-parser');

//Manipulação do BD---------------------------------------------------------------------------------------------------------------------------
var dbConn = require('./routes/conexaoBD');

var routesHorario = require('./routes/rotas-horarios');
app.use(routesHorario);

var routesUsuarios = require('./routes/rotas-usuarios');
app.use(routesUsuarios);

var routesConsultas = require('./routes/rotas-consultas');
app.use(routesConsultas);

app.set('view engine', 'pug');
app.set('views','./public/views');


//----------------------------------------------------------------------------------------------------------------------------------------------
//Manipulação dos arquivos estáticos
app.use(express.static('public'));

app.use("/styles", express.static(path.resolve('./public/css')));
app.use("/scripts", express.static(path.resolve('./public/js')));
//Manipulação das páginas

app.get('/', (req, res)=>{
	res.sendFile(path.join(__dirname + '/public/index.html'))
});

app.get('/home', (req, res)=>{
	res.sendFile(path.join(__dirname + '/public/index.html'))
});

app.get('/ver-horarios', (req, res)=>{
	res.sendFile(path.join(__dirname + '/public/indexHorarios.html'))
});

app.get('/criar-consulta', (req, res)=>{
	res.sendFile(path.join(__dirname + '/public/index_criarConsulta.html'))
});

app.get('/login', (req, res)=>{
	res.sendFile(path.join(__dirname + '/public/index_login.html'))
});

app.get('/cadastro-paciente', (req, res)=>{
	res.sendFile(path.join(__dirname + '/public/index_cadastro.html'))
});

app.listen(port, () => {
	console.log(`PUG Render em http://localhost:${port}`);
});
