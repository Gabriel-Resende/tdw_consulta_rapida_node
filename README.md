Comandos:
git status
git add -A
git commit -m “Sua mensagem”
git push


** Edite um arquivo, crie um novo arquivo e clone do Bitbucket em menos de 2 minutos **

Quando terminar, você pode excluir o conteúdo deste README e atualizar o arquivo com detalhes para outras pessoas que estão começando com o seu repositório.

* Recomendamos que você abra este README em outra guia enquanto executa as tarefas abaixo. Você pode [assistir ao nosso vídeo] (https://youtu.be/0ocf7u76WSo) para uma demonstração completa de todas as etapas deste tutorial. Abra o vídeo em uma nova guia para evitar sair do Bitbucket. *

---

## Editar um arquivo

Você começará editando este arquivo LEIA-ME para aprender como editar um arquivo no Bitbucket.

1. Clique em ** Fonte ** no lado esquerdo.
2. Clique no link README.md na lista de arquivos.
3. Clique no botão ** Editar **.
4. Exclua o seguinte texto: * Exclua esta linha para fazer uma alteração no README do Bitbucket. *
5. Depois de fazer sua alteração, clique em ** Commit ** e em ** Commit ** novamente na caixa de diálogo. A página de confirmação será aberta e você verá a alteração que acabou de fazer.
6. Volte para a página ** Fonte **.

---

## Criar um arquivo

Em seguida, você adicionará um novo arquivo a este repositório.

1. Clique no botão ** Novo arquivo ** na parte superior da página ** Fonte **.
2. Dê ao arquivo o nome de arquivo ** contributors.txt **.
3. Digite seu nome no espaço de arquivo vazio.
4. Clique em ** Commit ** e em ** Commit ** novamente na caixa de diálogo.
5. Volte para a página ** Fonte **.

Antes de prosseguir, vá em frente e explore o repositório. Você já viu a página ** Source **, mas verifique as páginas ** Commits **, ** Branches ** e ** Settings **.

---

## Clonar um repositório

Use estas etapas para clonar do SourceTree, nosso cliente para usar a linha de comando do repositório gratuitamente. A clonagem permite que você trabalhe em seus arquivos localmente. Se você ainda não possui o SourceTree, [baixe e instale primeiro] (https://www.sourcetreeapp.com/). Se você preferir clonar a partir da linha de comando, consulte [Clonar um repositório] (https://confluence.atlassian.com/x/4whODQ).

1. Você verá o botão clone sob o título ** Fonte **. Clique nesse botão.
2. Agora clique em ** Check out in SourceTree **. Pode ser necessário criar uma conta SourceTree ou fazer login.
3. Quando você vir a caixa de diálogo ** Clonar novo ** no SourceTree, atualize o caminho e o nome de destino se desejar e clique em ** Clonar **.
4. Abra o diretório que você acabou de criar para ver os arquivos do seu repositório.

Agora que você está mais familiarizado com seu repositório Bitbucket, vá em frente e adicione um novo arquivo localmente. Você pode [empurrar sua alteração de volta para o Bitbucket com SourceTree] (https://confluence.atlassian.com/x/iqyBMg), ou você pode [adicionar, confirmar] (https://confluence.atlassian.com/x/ 8QhODQ) e [push da linha de comando] (https://confluence.atlassian.com/x/NQ0zDQ).


