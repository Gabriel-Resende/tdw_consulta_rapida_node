<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Gabriel Elias Vieira da Silva Resende, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Conclusão</title>
    <link rel="sortcut icon" href=".\img\favicon.ico" type="image/x-icon"/>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/checkout/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/style.css">

    <!-- Custom styles for this template -->
    <link href="./css/form-validation.css" rel="stylesheet">
</head>
<body class="bg-light">
<div class="container">
    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="./img/logo.png" alt="" width="72" height="72">
        <h2>Conclusão da Consulta</h2>
        <p class="lead">Escolha o local, horário e médicos disponíveis para concluir a consulta.</p>
        <hr>
    </div>

    <div class="row">

        <div id="conteudo" class="col-md-8 order-md-1">
            <h4 id="title" class="bg-cinza">Dados Cadastrais</h4>


            <?php
            $emailUser = 0;
            $senhaUser = 0;
            $emailUser = $_GET["email"];
            $senhaUser = $_GET["senha"];

            $mysqli = new mysqli('localhost', 'root', '', 'consultarapida');
            $sql = "SELECT idUsuario, primeiroNome, sobrenome, email, senha FROM usuario WHERE (email = $emailUser and senha = $senhaUser) ";
            $query = $mysqli->query($sql);


            if ($emailUser =="adm@gmail.com" && $senhaUser == "12345678") {
            echo "<form name ='formConsulta' action='ConsultaConcluir.php' method='post' class ='was-validated'>";

                echo "<div class=\"mb-3\">
                    <select name=\"localidade\" class=\"custom-select\" required>
                        <option value =\"\">Localidade...</option>
                        <option value=\"1\">Casa Grande</option>
                        <option value=\"2\">Córrego Fundo</option>
                        <option value=\"3\">Cachoeira</option>
                    </select>
                    <div class=\"invalid-feedback\">Sem local selecionado.</div>
                </div>


                <div class=\"mb-3\">
                    <select name=\"horario\" class=\"custom-select\" required>
                        <option value=\"\">Horário...</option>
                        <option value=\"12:00pm\">12:00 pm</option>
                        <option value=\"7:30am\">7:30 am</option>
                        <option value=\"10am\">10:00 am</option>
                    </select>
                    <div class=\"invalid-feedback\">Sem horário selecionado.</div>
                </div>

                <div class=\"mb-3\">
                    <select class=\"custom-select\" required>
                        <option value=\"\">Médicos...</option>
                        <option value=\"1\">Dr. Armando</option>
                        <option value=\"2\">Dr. Cristiano</option>
                    </select>
                    <div class=\"invalid-feedback\">Sem médico selecionado.</div>
                </div>

                <button type='submit' class='text-dark btn btn-primary'>Enviar</button> ";
            echo "</form>";
            } else {
                echo "<p><strong>Email ou senha incorretos!!</strong></p>";
            }

            ?>

        </div>
    </div>
</div>
<script src="./js/Index.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="./js/bootstrap.bundle.min.js"></script>
        <script src="./js/form-validation.js"></script>-->
</body>
</html>
