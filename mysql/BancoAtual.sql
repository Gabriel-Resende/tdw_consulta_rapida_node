-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.7.24 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando dados para a tabela consultarapida.consulta: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `consulta` DISABLE KEYS */;
INSERT IGNORE INTO `consulta` (`idConsulta`, `idhorario`, `idpaciente`) VALUES
	(1, 2, 4),
	(2, 3, 8),
	(3, 5, 11),
	(5, 5, 8);
/*!40000 ALTER TABLE `consulta` ENABLE KEYS */;

-- Copiando dados para a tabela consultarapida.horarios: ~70 rows (aproximadamente)
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
INSERT IGNORE INTO `horarios` (`idhorarios`, `diaSemana`, `horario`, `localidade`, `nomeMedico`) VALUES
	(1, 'Quarta-feira', '12:00', 'Córrego Fundo', 'Dr. Armando'),
	(2, 'Quarta-feira', '08:00', 'Cachoeira', 'Dr. Armando'),
	(3, 'Segunda-feira', '08:00', 'Casa Grande', 'Dr. Cristiano '),
	(4, 'Quinta-feira', '15:00', 'Casa Grande', 'Dr. Armando'),
	(5, 'Segunda-feira', '12:45', 'Casa Grande', 'Dra. Elke'),
	(6, 'Terça-feira', '18:00', 'Córrego Fundo', 'Dr. Cristiano '),
	(7, 'Sábado', '12:00', 'Córrego Fundo', 'Dra. Glívia'),
	(8, 'Sexta-feira', '15:30', 'Cachoeira', 'Dr. Israel'),
	(9, 'Sexta-feira', '19:00', 'Casa Grande', 'Dr. Délcio'),
	(10, 'Terça-feira', '14:00', 'Cachoeira', 'Dr. Maurício'),
	(11, 'Sexta-feira', '15:00', 'Casa Grande', 'Dra. Elke'),
	(12, 'Sábado', '13:00', 'Córrego Fundo', 'Glívia'),
	(13, 'Quarta-feira', '15:15', 'Córrego Fundo', 'Dr. Francisco'),
	(14, 'Quarta-feira', '14:11', 'Córrego Fundo', 'Dra. Marli'),
	(15, 'Terça-feira', '12:00', 'Córrego Fundo', 'Dr. Ênio'),
	(17, 'Quarta-feira', '15:35', 'Casa Grande', 'Dr. Naum'),
	(18, 'Terça-feira', '08:30', 'Córrego Fundo', 'Dr. Arnold'),
	(19, 'Sábado', '15:30', 'Cachoeira', 'Dr. Weverson'),
	(23, 'Sábado', '14:00', 'Casa Grande', 'Dr. Moacir'),
	(24, 'Terça-feira', '15:30', 'Córrego Fundo', 'Dra. Elke'),
	(25, 'Terça-feira', '15:30', 'Córrego Fundo', 'Dra. Elke'),
	(26, 'Segunda-feira', '09:57', 'Cachoeira', 'Dr. Elvio'),
	(27, 'Sexta-feira', '12:00', 'Casa Grande', 'Dr. Elias'),
	(28, 'Quinta-feira', '10:30', 'Córrego Fundo', 'Dra. Letícia '),
	(29, 'Sábado', '15:30', 'Cachoeira', 'Dr. Francisco'),
	(30, 'Sexta-feira', '20:00', 'Cachoeira', 'Dr. Francisco'),
	(31, 'Quinta-feira', '15:30', 'Córrego Fundo', 'Dr. Israel'),
	(32, 'Quinta-feira', '15:30', 'Córrego Fundo', 'Dr. Chuck Norris'),
	(33, 'Quinta-feira', '15:30', 'Córrego Fundo', 'Dr. Israel'),
	(34, 'Quarta-feira', '15:12', 'Casa Grande', 'Dr. Armando'),
	(35, 'Segunda-feira', '07:30', 'Córrego Fundo', 'Dra. Glívia'),
	(36, 'Quinta-feira', '15:30', 'Casa Grande', 'Dr. Elias'),
	(37, 'Segunda-feira', '21:30', 'Casa Grande', 'Dr. André'),
	(38, 'Terça-feira', '14:24', 'Cachoeira', 'Dr. Márcio'),
	(39, 'Segunda-feira', '15:15', 'Casa Grande', 'Dr. Jean'),
	(40, 'Terça-feira', '10:03', 'Córrego Fundo', 'Dra. Ludimila'),
	(41, 'Quarta-feira', '08:30', 'Córrego Fundo', 'Dr. José'),
	(42, 'Quinta-feira', '15:01', 'Cachoeira', 'Dra. Mariana'),
	(43, 'Sexta-feira', '08:30', 'Casa Grande', 'Dra. Glívia'),
	(44, 'Quinta-feira', '15:01', 'Cachoeira', 'Dra. Mariana'),
	(45, 'Quinta-feira', '15:01', 'Cachoeira', 'Dra. Mariana'),
	(46, 'Quinta-feira', '15:01', 'Cachoeira', 'Dra. Mariana'),
	(47, 'Segunda-feira', '15:22', 'Casa Grande', 'Dr. Mário'),
	(48, 'Segunda-feira', '15:22', 'Casa Grande', 'Dr. Mário'),
	(49, 'Segunda-feira', '15:22', 'Casa Grande', 'Dr. Mário'),
	(50, 'Segunda-feira', '15:22', 'Casa Grande', 'Dr. Mário'),
	(51, 'Sexta-feira', '15:30', 'Casa Grande', 'Dr. Mário'),
	(52, 'Segunda-feira', '15:22', 'Casa Grande', 'Dr. Mário'),
	(53, 'Segunda-feira', '15:22', 'Casa Grande', 'Dr. Mário'),
	(54, 'Segunda-feira', '15:22', 'Casa Grande', 'Dr. Mário'),
	(55, 'Sexta-feira', '21:12', 'Córrego Fundo', 'Dra. Ana'),
	(56, 'Sábado', '12:00', 'Córrego Fundo', 'Dra. Mariana'),
	(57, 'Quarta-feira', '15:30', 'Córrego Fundo', 'Dra. Mara'),
	(58, 'Segunda-feira', '18:00', 'Córrego Fundo', 'Dr. Francisco'),
	(59, 'Terça-feira', '14:32', 'Cachoeira', 'Dra. Julia'),
	(60, 'Quarta-feira', '17:58', 'Casa Grande', 'Dra. Glívia'),
	(61, 'Quinta-feira', '08:30', 'Córrego Fundo', 'Dra. Marli'),
	(62, 'Sexta-feira', '12:14', 'Cachoeira', 'Dr. Franco'),
	(63, 'Sexta-feira', '12:14', 'Cachoeira', 'Dr. Franco'),
	(64, 'Quinta-feira', '10:02', 'Córrego Fundo', 'Dr. Everton'),
	(65, 'Quinta-feira', '10:02', 'Córrego Fundo', 'Dr. Everton'),
	(66, 'Quinta-feira', '10:02', 'Córrego Fundo', 'Dr. Everton'),
	(67, 'Quinta-feira', '08:30', 'Cachoeira', 'Dr. Everton');
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;

-- Copiando dados para a tabela consultarapida.usuario: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT IGNORE INTO `usuario` (`idUsuario`, `primeiroNome`, `sobrenome`, `email`, `senha`, `telefone`, `endereco`, `cidade`, `regiao`, `idade`, `cartaoSUS`, `cpf`, `sexo`) VALUES
	(1, 'Gabriel Elias', 'Vieira da Silva Resende', 'gabriel.resende.980610@gmail.com', '980610', '031997112298', 'Sitio Rincão das Goiabas, Zona Rural', 'Casa Grande', 'Córrego Fundo', 22, '798 2411 6131 8464', '137.798.746-95', 'Masculino'),
	(2, 'Marli Elias', 'da Silva Resende', 'enio.resende.1040578@gmail.com', 'adsadafgsgsges', '031997111298', 'Sitio RincÃ£o das Goiabas, Zona Rural', 'Casa Grande', 'Córrego Fundo', 49, '344 6378 4636 4363', '822.547.862-15', 'Masculino'),
	(3, 'Ênio José', 'Vieira de Resende', 'enio.resende.100578@gmail.com', 'dffhdh', '031997111998', 'Sitio RincÃ£o das Goiabas, Zona Rural', 'Casa Grande', 'Córrego Fundo', 48, '665 5555 4848 4811', '791.116.826-49', 'Masculino'),
	(4, 'Glívia', 'Diniz Resende', 'gliviadiniz@gmail.com', 'meuamor', '031971259974', 'Sítio', 'Casa Grande', 'Córrego Fundo', 21, '435 5433 6335 6356', '257.455.151-46', 'Feminino'),
	(6, 'Maria Elias', 'da Silva', 'gadgda@sgsfg.com', 'sfsdsd', '039484515158', 'Casa Sua', 'Casa Grande', 'Casa Grande', 81, '166 1464 5848 5454', '311.651.661-61', 'Feminino'),
	(7, 'José', 'Messi', 'messi.melhor@futebol.com', '2020melhor', '021998989566', 'Catalunia', 'Casa Grande', 'Casa Grande', 31, '611 6165 5161 6169', '444.444.444-44', 'Masculino'),
	(8, 'João', 'Augusto', 'joaosilva@yahoo.com', 'sgsfgsgsfg', '024448466161', 'Pau de Óleo', 'Casa Grande', 'Cachoeira', 83, '311 1151 1121 3212', '214.723.642-22', 'Masculino'),
	(9, 'Habdul', 'Hussein', 'gabriel.resende.7127@facebook.com', 'asfafsf', '031998884848', 'Fazenda', 'Casa Grande', 'Cachoeira', 35, '798 2411 6131 8464', '616.165.165-15', 'Masculino'),
	(10, 'João', 'Augusto da Silva', 'joao.augusto@gmail.com', '12345678', '032997157515', 'Zona rural', 'Casa Grande', 'Córrego Fundo', 84, '435 5445 6416 484', '792.116.826-45', 'Masculino'),
	(11, 'Marlon', 'Chaves', 'marlon.chaves.1778@gmail.com', 'adssfa466se', '031998728208', 'São Paulo, Capital', 'Casa Grande', 'Córrego Fundo', 25, '478 8115 0175 9740', '789.103.762-25', 'Masculino'),
	(12, 'João', 'Nesburgo', 'jnes@bus.com', 'asdwdasffsaw', '041 9 3357 8954', 'Capital', 'Casa Grande', 'Córrego Fundo', 47, '654 6464 6116 1616', '516.160.616-51', 'Masculino'),
	(13, 'Myke', 'Tison', 'myke.tison@boxe.com', 'sdgsgsgegsddsgsdggdsges', '078 9 9595 4616', 'Califórnia', 'Outra...', 'Outra...', 53, '665 4646 1161 1165', '646.116.613-13', 'Masculino'),
	(14, 'Marisa', 'Letícia', 'malet@gami.com', 'gds46161sg6sgsdg', '082 9 7758 4845', 'Brasília Amarela', 'Outra...', 'Outra...', 68, '464 6464 6846 1564', '464.668.166-56', 'Feminino'),
	(15, 'Queila', 'Renata da Silva', 'Queila.renata@gmail.com', 'aughugslhghg67', '', 'Pau de Óleo', 'Outra...', 'Outra...', 39, '165 1651 6189 161', '151.518.451-21', 'Feminino'),
	(17, 'Queila', 'Mariana', 'queila.mariana@gmail.com', '].sfesf4616164', '035 5 4845 1566', 'Paulo XI', 'Outra...', 'Outra...', 20, '184 8451 5161 6516', '826.262.626-26', 'Feminino'),
	(18, 'João', 'Pé de Feijão', 'jpdf@sdfdf.sff', '44661616416161681616861616161816186161', '031 8 5484 5115', 'Feijão', 'Casa Grande', 'Córrego Fundo', 19, '751 9616 4498 6033', '137.798.746-93', 'Não Respondeu'),
	(19, 'Anderson', 'Silva', 'thespider@ufc.com', 'ufcspider10355416161661', '031 9 8484 4646', 'Octógono', 'Casa Grande', 'Outra...', 42, '845 1111 6201 6161', '616.616.843-31', 'Masculino'),
	(20, 'Margarete', 'Silva Diniz ', 'silvamarg.cas@gmail.com', 'afsdfsefs5f16s1df61e6f1s61e61fs61f', '032 9 8878 4515', 'Rua Palmas', 'Outra...', 'Outra...', 48, '182 8426 9164 9844', '441.514.646-11', 'Masculino'),
	(23, 'Margarete', ' Diniz ', 'silvamsaarg.cas@gmail.com', 'asdwadawd', '032 9 9878 4515', 'Rua Palmas', 'Outra...', 'Casa Grande', 48, '851 4649 8512 3398', '4846-11.451-51', 'Feminino'),
	(24, 'Marcus ', 'Assunção', 'marcosassuncao@sdf.com', '4566586464', '099 8 9841 5151', 'Paleiras', 'Casa Grande', 'Outra...', 48, '665 5555 4848 4811', '915.648.461-61', 'Masculino'),
	(25, 'Naruto', 'Uzumaki', 'konahareviews.cds@bandai.com', '455651651651611', '031 8 4677 4619', 'Konoha', 'Outra...', 'Outra...', 30, '745 4546 8461 6846', '464.648.463-13', 'Masculino'),
	(27, 'Sakura', 'Haruno', 'konahaSakurareviews.cds@bandai.com', '616316846131561616', '031 8 4677 4617', 'Konoha', 'Casa Grande', 'Casa Grande', 30, '745 4546 4848 4866', '4863-13.787-98', 'Feminino'),
	(28, 'Sem ', 'Ideia', 'semideia@gmail.com', 'dsssegegsg', '099 9 9999 9999', 'Sweet', 'Casa Grande', 'Casa Grande', 22, '611 6165 5161 6169', '137.798.746-97', 'Não Respondeu'),
	(29, 'Carl', 'Johnson', 'cj.groove@san.com', '46464163131', '055 9 5554 5485', 'Groove Street', 'Casa Grande', 'Casa Grande', 78, '121 5164 1653 1384', '051.684.548-64', 'Masculino'),
	(30, 'Big', 'Smoke', 'big.smoke.grblsq@gmail.com', 'fsdfbkj ackclsklm', '058 4 3168 1354', 'Santa Mônica', 'Casa Grande', 'Córrego Fundo', 78, '665 5575 4848 4811', '484.824.184-14', 'Masculino'),
	(31, 'Teste', 'Email', 'teste.email@gmail.com', '12345678', '031 1 6516 4646', 'Casa onde mora', 'Casa Grande', 'Córrego Fundo', 10, '548 4984 6543 2194', '135.487.667-41', 'Masculino'),
	(32, 'Donald', 'Trump', 'donald.trump.3423@eua.com', 'trump', '055 9 8484 8464', 'Casa Branca', 'Casa Grande', 'Córrego Fundo', 53, '574 6813 4646 1113', '005.184.846-44', 'Masculino');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
